<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('alumnes', function (Blueprint $table) {
            $table->bigIncrements("idAlumne");
            $table->string("nom",50);
            $table->string("cognoms",50);
            $table->string("DNI",10);
            $table->string("correu",20);
            $table->string("telefon",15);
            $table->integer("curs");
            $table->foreignId('idUser')->nullable()->constrained('users')->references('id');
            $table->foreignId('idEstudi')->nullable()->constrained('estudis')->references('idEstudi');
            $table->string("fitxer_cv",50)->nullable();
            $table->timestamps();
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alumnes', function (Blueprint $table) {
            $table->dropForeign(['alumnes_idUser_foreign']);
            $table->dropColumn('idUser');
            $table->dropForeign(['alumnes_idEstudi_foreign']);
            $table->dropColumn('idEstudi');
        });
        Schema::dropIfExists('alumnes');
    }
};
