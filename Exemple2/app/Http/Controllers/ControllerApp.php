<?php

namespace App\Http\Controllers;


use App\Models\Alumnes;
use App\Models\Empreses;
use App\Models\Enviaments;
use App\Models\Estudis;
use App\Models\Ofertes;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\Rule;
use Ramsey\Uuid\Type\Integer;
use function MongoDB\BSON\toJSON;

class ControllerApp extends Controller
{
    //
    public function getAllUsers(){
        $tots = User::all();
        return $tots->toJson();
    }

    public function index()
    {
        $user = User::findOrFail(Auth::user()->id);

        //$alumnes = Alumnes::;
        $alumnes = Alumnes::Select('idAlumne')-> where('idUser', Auth::user()->id)->get();
        /*$alu=array();
        foreach ($alumnes as $alumne){
            array_push($alu,$alumne->idAlumne);
        }*/
        //$enviaments = Enviaments::whereIn('idAlumne', $alumnes)->whereIn('estat',array('NoConveni','Acceptat'))->get();
        $enviaments = Enviaments::addSelect(['nom' => Alumnes::select('nom')
            -> whereColumn('idAlumne','enviaments.idAlumne' )])
            ->addSelect(['cognoms' => Alumnes::select('cognoms')
                -> whereColumn('idAlumne','enviaments.idAlumne' )])
            ->addSelect(['descripcio' => Ofertes::select('descripcio')
                -> whereColumn('idOferta','enviaments.idOferta' )])
            ->whereIn('idAlumne', $alumnes)
            ->whereIn('estat',array('NoConveni','Acceptat'))->paginate(5);

        //return $enviaments->toJSON();
        return view('home2', [
            'user' => $user,
            'enviaments' => $enviaments
        ]);
        // Refrescar logout
        //Auth::logout();

    }
}
