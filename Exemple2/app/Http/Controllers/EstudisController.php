<?php

namespace App\Http\Controllers;

use App\Models\Estudis;
use Illuminate\Http\Request;

class EstudisController extends Controller
{
    public function getAllEstudis(){
        $tots = Estudis::all();
        return $tots->toJson();
    }
}
