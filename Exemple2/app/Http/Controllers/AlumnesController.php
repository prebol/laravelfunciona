<?php

namespace App\Http\Controllers;

use App\Models\Alumnes;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PhpParser\Node\Scalar\String_;

class AlumnesController extends Controller
{
    public function nouAlumne() {
        return view('nouAlumne');
    }


    public function addAlumne(Request $request) {
        $user = User::findOrFail(Auth::user()->id);
        $alumne = new Alumnes();
        $file = $request->file('file');
        $extension = $file->getClientOriginalName();



        $alumne = new Alumnes();
        $alumne->fitxer_cv=$extension;
        $alumne->nom = $request->input('nom');
        $alumne->cognoms = $request->input('cognoms');
        $alumne->DNI = $request->input('DNI');
        $alumne->correu = $request->input('correu');
        $alumne->telefon = $request->input('telefon');
        if($request->input('curs')!=null)
        {
            $alumne->curs = $request->input('curs');
        }
        else
        {
            $alumne->curs = $user->curs;
        }
        $alumne->idUser = $user->id;
        $alumne->idEstudi = $request->input('idEstudi');

        $alumne->save();

        return redirect('/alumnes')->with('success', "L'Alumne ".$alumne->nom." ha estat afegit correctament.");
    }

    public function getAllAlumnes(){
        $alumnes = Alumnes::all();
        $user = User::findOrFail(Auth::user()->id);
        return view('alumnes', [
            'user' => $user,
            'alumnes' => $alumnes
        ]);
    }
}
