<?php

namespace App\Http\Controllers;

use App\Models\Empreses;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class EmpresaController extends Controller
{
    public function getAllEmpreses(){
        $user = User::findOrFail(Auth::user()->id);
        $empreses = Empreses::paginate(5);
        //return $tots->toJson();
        return view('empresa', [
            'user' => $user,
            'empreses' => $empreses
        ]);

    }
    public function novaEmpresa() {
        return view('novaEmpresa');
    }
    public function addEmpresa(Request $request) {
        $validation =  $this->validate($request, [
            'correu' => ['required', 'string', 'email', 'min:5', 'max:100', 'unique:empreses'],
        ]);
        if($request->hasFile('file')){
            $file = $request->file('file');
            $extension = $file->getClientOriginalExtension();
        }

        $empresa = new Empreses();
        $empresa->nom = $request->input('nom');
        $empresa->adreca = $request->input('adreca');
        $empresa->telefon = $request->input('telefon');
        $empresa->correu = $request->input('correu');

        $empresa->save();

        return redirect(route('empresa'))->with('success', "L'empresa ".$empresa->nom." ha estat afegida correctament.");
    }
    public function editEmpresa($id) {

        $empresa = Empreses::findOrFail($id);
        return view('editEmpresa',['empresa' => $empresa]);
    }
    public function guardarEmpresa(Request $request) {
        $id = intval($request->input('idEmpresa'));

        //$request = Route::current();
        $validation =  $this->validate($request, [
            'nom' => ['required', 'string', 'min:1', 'max:255'],
            'adreca' => ['required', 'string', 'max:255'],
            'telefon' => ['required', 'string', 'max:15'],
            'correu' => ['required', 'string', 'email', 'min:5', 'max:100', Rule::unique('empreses')->ignore($id,"idEmpresa")],
        ]);

        $empresa = Empreses::findOrFail($id);


        $empresa->nom = $request->input('nom');
        $empresa->adreca = $request->input('adreca');
        $empresa->telefon = $request->input('nom');
        $empresa->correu = $request->input('correu');

        $empresa->update();
        return redirect(route('empresa'))->with('success', "L'empresa ".$empresa->nom." ha estat guardat correctament.");

    }
}
