<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ofertes extends Model
{
    use HasFactory;
    protected $table = "ofertes";
    protected $primaryKey = "idOferta";
    protected $fillable = ["idOferta","descripcio", "curs", "nomcontacte", "cognomscontacte", "correucontacte", "numVacants", "idEmpresa"];

    public function estudis(){
        return $this->BelongsTo(Estudis::class, 'idEstudis', 'idEstudis');
    }
    public function empreses(){
        return $this->BelongsTo(Empreses::class, 'idEmpresa', 'idEmpresa');
    }
    public function alumnes(){
        return $this->belongsToMany(Alumnes::class, "enviaments","idAlumne","idOferta")->withTimestamps();
    }
}
