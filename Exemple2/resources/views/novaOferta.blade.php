<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-md-8">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>ERROR!</strong> T'has equivocat!!!
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">{{ __('newOferta') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form  method="POST" action="/novaOferta">
                            @csrf
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="Descripcio">Descripcion</label>
                                    <input type="text" name="desc" class="form-control" id="Desc" placeholder="Descripcion" required>
                                </div>
                                <div class="form-group">
                                    <label for="adreca">curso</label>
                                    <input type="text" name="curs" class="form-control" id="curs" placeholder="curs" required>
                                </div>
                                <div class="form-group">
                                    <label for="telefon">Telèfon</label>
                                    <input type="text" name="nom" class="form-control" id="nom" placeholder="nom" required>
                                </div>
                                <div class="form-group">
                                    <label for="telefon">cognom</label>
                                    <input type="text" name="cognom" class="form-control" id="cognom" placeholder="cognom" required>
                                </div>
                                <div class="form-group mt-4">
                                    <label for="correu">Correu</label>
                                    <input type="email" name="correu" class="form-control" id="correu"  placeholder="Correu" required>
                                </div>
                                <div class="form-group mt-4">
                                    <label for="correu">Estudi</label>
                                    <input type="text" name="Estudi" class="form-control" id="Estudi"  placeholder="Estudi" required>
                                </div>
                                <div class="form-group mt-4">
                                    <label for="correu">Empresa</label>
                                    <input type="text" name="Empresa" class="form-control" id="Empresa"  placeholder="Empresa" required value={{$oferta->idEmpresa}}>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary mt-4">Guardar</button>
                            <a title="Cancelar" class="btn btn-primary mt-4" href="{{ route('empresa')}}">

                                Cancel·lar

                            </a>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
</body>
</html>
