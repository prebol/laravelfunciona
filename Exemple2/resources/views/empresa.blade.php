<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
@extends('layouts.app')
@section('content')
    <div class="container">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                <strong>{{ $message }}</strong>

            </div>

        @endif
        <div class="card">
            <div class="card-header">{{ __('Empreses') }}</div>


            <div class="card-body">

                <table class="table table-striped table-hover mb-5">
                    <thead>
                    <tr>
                        <th>ID Empresa</th>
                        <th>Nom</th>
                        <th>Adreça</th>
                        <th>Telèfon</th>
                        <th>Correu</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($empreses as $empresa)
                        <tr id="{{$empresa->idEmpresa}}">
                            <td> {{$empresa->idEmpresa}} </td>
                            <td> {{$empresa->nom}} </td>
                            <td> {{$empresa->adreca}} </td>
                            <td> {{$empresa->telefon}} </td>
                            <td> {{$empresa->correu}} </td>

                            @if($user->grup=="coord")
                            <td>
                                <a title="Editar Empresa" class="btn btn-primary" href="{{ route('editEmpresa',[$empresa->idEmpresa]) }}">

                                    Editar

                                </a>
                            </td>
                            <td>
                                <a type="button" title="Afegir oferta" class="btn btn-primary" href="empresa/oferta/add/{{$empresa->idEmpresa}}">
                                Nova Oferta
                                </a>
                            </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                @if($user->grup=="coord")
                <a title="Afegir Empresa" class="btn btn-primary" href="{{ route('novaEmpresa')}}">

                    Nova Empresa

                </a>
                @endif
                {{-- Pagination --}}
                <div class="d-flex justify-content-center">
                    {{ $empreses->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection

</body>
</html>
