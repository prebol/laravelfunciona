@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                        <nav class="menu">
                            <ul>
                                <li><a href="/empresa">Empresas</a></li>
                                <li><a href="{{ route('oferta') }}">Ofertas</a></li>
                                @if($user->grup=="tutor")
                                <li><a href="/alumnes">Alumnos</a></li>
                                @endif
                            </ul>
                        </nav>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
